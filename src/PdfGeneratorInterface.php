<?php

namespace Drupal\phpwkhtmltopdf;

use Drupal\phpwkhtmltopdf\Entity\PdfSettings;

/**
 * Generates PDFs.
 */
interface PdfGeneratorInterface {

  /**
   * Returns the file name of the pdf document.
   *
   * @return string
   *   The file name.
   */
  public function getFileName();

  /**
   * Sets the file name for the pdf document.
   *
   * @param string $fileName
   *   The file name.
   *
   * @return $this
   */
  public function setFileName($fileName);

  /**
   * Sets the pdf settings config entity.
   *
   * @param \Drupal\phpwkhtmltopdf\Entity\PdfSettings $pdfSettings
   *   The pdf settings.
   */
  public function setPdfSettings(PdfSettings $pdfSettings);

  /**
   * Generates a pdf from internal drupal url.
   *
   * @param string $url
   *   The url to get the html from.
   * @param bool $returnAsString
   *   (optional) Set to TRUE to get the pdf as a string.
   *
   * @return void|string
   *   Returns the pdf string or sends the pdf to the user.
   */
  public function generateFromUrl($url, $returnAsString = FALSE);

  /**
   * Generates a pdf from an entity.
   *
   * @param int $entityId
   *   The entity id.
   * @param string $entityType
   *   The entity type.
   * @param string $viewMode
   *   (optional) The view mode that should be used to render the entity.
   * @param bool $returnAsString
   *   (optional) Set to TRUE to get the pdf as a string.
   *
   * @return void|string
   *   Returns the pdf string or sends the pdf to the user.
   */
  public function generateFromEntity($entityId, $entityType, $viewMode = 'full', $returnAsString = FALSE);

  /**
   * Generates a pdf from html.
   *
   * @param bool $returnAsString
   *   (optional) Set to TRUE to get the pdf as a string.
   *
   * @return void|string
   *   Returns the pdf string or sends the pdf to the user.
   */
  public function generateFromHtml($returnAsString = FALSE);

  /**
   * Adds another page with the specified html.
   *
   * @param string $html
   *   The html.
   * @param array $options
   *   The page specifig options.
   */
  public function addPage($html, array $options = []);

}
