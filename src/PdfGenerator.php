<?php

namespace Drupal\phpwkhtmltopdf;

use Drupal\Component\Utility\Random;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\phpwkhtmltopdf\Entity\PdfSettings;
use Drupal\phpwkhtmltopdf\Exception\MissingPdfSettingsEntity;
use Drupal\phpwkhtmltopdf\Exception\PdfGenerationException;
use GuzzleHttp\ClientInterface;
use mikehaertl\wkhtmlto\Pdf;
use PharIo\Manifest\InvalidUrlException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Generates PDFs.
 */
class PdfGenerator implements PdfGeneratorInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * This class is a slim wrapper around wkhtmltopdf.
   *
   * @var \mikehaertl\wkhtmlto\Pdf
   */
  protected $htmlToPdf;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The file name of the pdf.
   *
   * @var string
   */
  protected $fileName;

  /**
   * An HTTP kernel for making subrequests.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The pdf settings config entity.
   *
   * @var \Drupal\phpwkhtmltopdf\Entity\PdfSettings
   */
  protected $pdfSettings;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $requestStack;

  /**
   * The filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new PdfGenerator object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The HTTP Kernel service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request object.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The filesystem.
   */
  public function __construct(ClientInterface $http_client, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, HttpKernelInterface $http_kernel, RequestStack $request_stack, FileSystemInterface $fileSystem) {
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->httpKernel = $http_kernel;

    $this->htmlToPdf = new Pdf();
    $this->requestStack = $request_stack;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileName() {
    return $this->fileName;
  }

  /**
   * {@inheritdoc}
   */
  public function setFileName($fileName) {
    $this->fileName = $fileName . '.pdf';

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPdfSettings(PdfSettings $pdfSettings) {
    $this->pdfSettings = $pdfSettings;
  }

  /**
   * Generates a pdf.
   *
   * @param bool $returnAsString
   *   Set to TRUE to get the pdf as a string.
   *
   * @return void|string
   *   Returns the pdf string or sends the pdf to the user.
   */
  protected function generatePdf($returnAsString = FALSE) {
    if (!$this->pdfSettings) {
      throw new MissingPdfSettingsEntity('Set the pdf settings config entity before using generate pdf.');
    }

    $settings = $this->pdfSettings->getAllGlobalSettings();
    $this->htmlToPdf->setOptions($settings);

    if (!$this->fileName) {
      $randomGenerator = new Random();
      $this->fileName = $randomGenerator->name(5) . '.pdf';
    }

    if ($returnAsString) {
      return $this->htmlToPdf->toString();
    }

    $tempDir = $this->fileSystem->getTempDirectory();
    $path = $tempDir . '/' . $this->fileName;
    if (!$this->htmlToPdf->saveAs($path)) {
      throw new PdfGenerationException($this->htmlToPdf->getError());
    }

    $response = new BinaryFileResponse($path);
    $response->setContentDisposition('attachment', $this->fileName);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromEntity($entity, $entityType, $viewMode = 'full', $returnAsString = FALSE) {
    $viewBuilder = $this->entityTypeManager->getViewBuilder($entityType);

    $entityBuild = $viewBuilder->view($entity, $viewMode);
    $entityHtml = $this->renderer->renderPlain($entityBuild)->__toString();
    $this->addPage($entityHtml);

    return $this->generatePdf($returnAsString);
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromUrl($url, $returnAsString = FALSE) {
    if (!UrlHelper::isValid($url) || UrlHelper::isExternal($url)) {
      throw new InvalidUrlException('Pdf generator only allows internal relative paths.');
    }

    $uri = Url::fromUri('internal:' . $url)->toString();

    $sub_request = Request::create($uri, 'GET');
    $subResponse = $this->httpKernel->handle($sub_request, HttpKernelInterface::SUB_REQUEST);
    $html = $subResponse->getContent();

    // Get the host for the current environment.
    $host = $this->requestStack->getSchemeAndHttpHost();

    // We need to add the base tag to get everything working in docker.
    if (Settings::get('development')) {
      $host = "http://nginx/";
    }
    $html = str_replace('<head>', '<head><base href="' . $host . '">', $html);

    $this->addPage($html);

    return $this->generatePdf($returnAsString);
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromHtml($returnAsString = FALSE) {
    return $this->generatePdf($returnAsString);
  }

  /**
   * {@inheritdoc}
   */
  public function addPage($html, $options = []) {
    $this->htmlToPdf->addPage($html, $options);
  }

}
