<?php

namespace Drupal\phpwkhtmltopdf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Pdf settings entity.
 *
 * @ConfigEntityType(
 *   id = "phpwkhtmltopdf_pdf_settings",
 *   label = @Translation("Pdf settings"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\phpwkhtmltopdf\PdfSettingsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\phpwkhtmltopdf\Form\PdfSettingsForm",
 *       "edit" = "Drupal\phpwkhtmltopdf\Form\PdfSettingsForm",
 *       "delete" = "Drupal\phpwkhtmltopdf\Form\PdfSettingsDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\phpwkhtmltopdf\PdfSettingsHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "phpwkhtmltopdf_pdf_settings",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pdf_settings/{phpwkhtmltopdf_pdf_settings}",
 *     "add-form" = "/admin/structure/pdf_settings/add",
 *     "edit-form" = "/admin/structure/pdf_settings/{phpwkhtmltopdf_pdf_settings}/edit",
 *     "delete-form" = "/admin/structure/pdf_settings/{phpwkhtmltopdf_pdf_settings}/delete",
 *     "collection" = "/admin/structure/pdf_settings"
 *   }
 * )
 */
class PdfSettings extends ConfigEntityBase implements PdfSettingsInterface {

  /**
   * The Pdf settings ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Pdf settings label.
   *
   * @var string
   */
  protected $label;

  /**
   * The page size.
   *
   * @var string
   */
  protected $pageSize;

  /**
   * The orientation.
   *
   * @var string
   */
  protected $orientation;

  /**
   * The margin bottom.
   *
   * @var string
   */
  protected $marginBottom;

  /**
   * The margin left.
   *
   * @var string
   */
  protected $marginLeft;

  /**
   * The margin right.
   *
   * @var string
   */
  protected $marginRight;

  /**
   * The margin top.
   *
   * @var string
   */
  protected $marginTop;

  /**
   * Disables warnings.
   *
   * @var bool
   */
  protected $ignoreWarnings;

  /**
   * Returns the page size.
   *
   * @return string
   *   The page size.
   */
  public function getPageSize() {
    return $this->pageSize;
  }

  /**
   * Sets the page size.
   *
   * @param string $pageSize
   *   The page size.
   *
   * @return $this
   */
  public function setPageSize($pageSize) {
    $this->pageSize = $pageSize;

    return $this;
  }

  /**
   * Returns the orientation.
   *
   * @return string
   *   The orientation.
   */
  public function getOrientation() {
    return $this->orientation;
  }

  /**
   * Sets the orientation.
   *
   * @param string $orientation
   *   The orientation.
   *
   * @return $this
   */
  public function setOrientation($orientation) {
    $this->orientation = $orientation;

    return $this;
  }

  /**
   * Returns the margin bottom.
   *
   * @return string
   *   The margin bottom.
   */
  public function getMarginBottom() {
    return $this->marginBottom;
  }

  /**
   * Sets the margin bottom.
   *
   * @param string $marginBottom
   *   The maring bottom.
   *
   * @return $this
   */
  public function setMarginBottom($marginBottom) {
    $this->marginBottom = $marginBottom;

    return $this;
  }

  /**
   * Returns the margin left.
   *
   * @return string
   *   The margin left.
   */
  public function getMarginLeft() {
    return $this->marginLeft;
  }

  /**
   * Sets the margin left.
   *
   * @param string $marginLeft
   *   The margin left.
   *
   * @return $this
   */
  public function setMarginLeft($marginLeft) {
    $this->marginLeft = $marginLeft;

    return $this;
  }

  /**
   * Returns the margin right.
   *
   * @return string
   *   The margin right.
   */
  public function getMarginRight() {
    return $this->marginRight;
  }

  /**
   * Sets the margin right.
   *
   * @param string $marginRight
   *   The margin right.
   *
   * @return $this
   */
  public function setMarginRight($marginRight) {
    $this->marginRight = $marginRight;

    return $this;
  }

  /**
   * Returns the margin top.
   *
   * @return string
   *   The margin top.
   */
  public function getMarginTop() {
    return $this->marginTop;
  }

  /**
   * Sets the margin top.
   *
   * @param string $marginTop
   *   The margin top.
   *
   * @return $this
   */
  public function setMarginTop($marginTop) {
    $this->marginTop = $marginTop;

    return $this;
  }

  /**
   * Returns the ignore warning parameter.
   *
   * @return bool
   *   TRUE if we ignore warnings.
   */
  public function getIgnoreWarnings() {
    return $this->ignoreWarnings;
  }

  /**
   * Sets the ignore warnings parameter.
   *
   * @param bool $ignoreWarnings
   *   TRUE if we want to ignore warnings.
   *
   * @return $this
   */
  public function setIgnoreWarnings($ignoreWarnings) {
    $this->ignoreWarnings = $ignoreWarnings;

    return $this;
  }

  /**
   * Returns all the global settings for wkhtmltopdf.
   *
   * @return array
   *   The global settings keyed by settings name in wkhtmltopdf.
   */
  public function getAllGlobalSettings() {
    return [
      'page-size' => $this->getPageSize(),
      'orientation' => $this->getOrientation(),
      'margin-bottom' => $this->getMarginBottom(),
      'margin-left' => $this->getMarginLeft(),
      'margin-right' => $this->getMarginRight(),
      'margin-top' => $this->getMarginTop(),
      'ignoreWarnings' => $this->getIgnoreWarnings(),
    ];
  }

}
