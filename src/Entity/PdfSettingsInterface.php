<?php

namespace Drupal\phpwkhtmltopdf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Pdf settings entities.
 */
interface PdfSettingsInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
