<?php

namespace Drupal\phpwkhtmltopdf\Exception;

/**
 * Defines an exception to throw if an error occurs during a pdf generation.
 */
class PdfGenerationException extends \RuntimeException {

}
