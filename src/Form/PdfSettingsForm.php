<?php

namespace Drupal\phpwkhtmltopdf\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form class for pdf generator settings.
 */
class PdfSettingsForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $pdf_settings = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $pdf_settings->label(),
      '#description' => $this->t("Label for the PDF settings."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $pdf_settings->id(),
      '#machine_name' => [
        'exists' => '\Drupal\phpwkhtmltopdf\Entity\PdfSettings::load',
      ],
      '#disabled' => !$pdf_settings->isNew(),
    ];

    $form['global_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Global PDF settings'),
    ];

    $form['global_settings']['pageSize'] = [
      '#type' => 'select',
      '#title' => $this->t('Page size'),
      '#default_value' => $pdf_settings->getPageSize(),
      '#options' => [
        'Letter' => $this->t('Letter'),
        'A4' => $this->t('A4'),
      ],
    ];

    $form['global_settings']['orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#options' => [
        'Landscape' => $this->t('Landscape'),
        'Portrait' => $this->t('Portrait'),
      ],
      '#default_value' => $pdf_settings->getOrientation(),
    ];

    $form['global_settings']['marginBottom'] = [
      '#title' => $this->t('Bottom'),
      '#type' => 'textfield',
      '#size' => 5,
      '#field_suffix' => 'px',
      '#default_value' => $pdf_settings->getMarginBottom(),
    ];

    $form['global_settings']['marginLeft'] = [
      '#title' => $this->t('Left'),
      '#type' => 'textfield',
      '#size' => 5,
      '#field_suffix' => 'px',
      '#default_value' => $pdf_settings->getMarginLeft(),
    ];

    $form['global_settings']['marginRight'] = [
      '#title' => $this->t('Right'),
      '#type' => 'textfield',
      '#size' => 5,
      '#field_suffix' => 'px',
      '#default_value' => $pdf_settings->getMarginRight(),
    ];

    $form['global_settings']['marginTop'] = [
      '#title' => $this->t('Top'),
      '#type' => 'textfield',
      '#size' => 5,
      '#field_suffix' => 'px',
      '#default_value' => $pdf_settings->getMarginTop(),
    ];

    $form['global_settings']['ignoreWarnings'] = [
      '#title' => $this->t('Ignore warnings'),
      '#type' => 'checkbox',
      '#description' => $this->t('Whether to ignore any errors if a PDF file was still created.'),
      '#default_value' => $pdf_settings->getIgnoreWarnings(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $pdf_settings = $this->entity;
    $status = $pdf_settings->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label PDF settings.', [
          '%label' => $pdf_settings->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label PDF settings.', [
          '%label' => $pdf_settings->label(),
        ]));
    }
    $form_state->setRedirectUrl($pdf_settings->toUrl('collection'));
  }

}
