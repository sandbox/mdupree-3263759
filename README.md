phpwkhtmltopdf

PDF GENERATOR
===================

Provides the settings, form generation and handling of PDF documents.


Introduction
============

The phpwkhtmltopdf PDF Generator module generates, configure and display PDF files.


Requirements
============

This module requires no modules outside of Drupal core.


Installation Requirements
=========================


Configuration Requirements
==========================

 * Customize the PDF settings at Home » Administration » Structure » Pdf
  Settings.
